extern crate joker_jpg_lib;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::process;

use joker_jpg_lib::jpg_parse::{is_jpg, read_comment};

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        print!("inccorect arguments");
        process::exit(1);
    }
    let path = std::path::Path::new(args.get(1).unwrap());
    let mut file = File::open(path).unwrap();
    let mut filedata = Vec::<u8>::new();
    file.read_to_end(&mut filedata).unwrap();
    let mut content = filedata.as_mut_slice();
 
}
