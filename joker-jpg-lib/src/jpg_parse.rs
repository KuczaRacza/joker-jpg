use std::io::{Read, Seek, SeekFrom};

use crate::utils::jpg_errors;
use crate::utils::ty2;
use bincode::de;
use byteorder::{BigEndian, ReadBytesExt};
use std::io::Cursor;

#[derive(Clone)]

pub enum PixelFormat {
    YUV420,
}
impl Default for PixelFormat {
    fn default() -> Self {
        Self::YUV420
    }
}

#[derive(Clone, Copy, Default)]
pub struct ComponentSpecificParams {
    pub component_id: u8,
    //chroma shubsampling precision
    //in jpg hsample is only 4 bits long
    pub hsample: u8,
    pub vsample: u8,
    //selcted quantization table
    pub quantzation_table: u8,
}
impl ComponentSpecificParams {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }
}

#[derive(Clone, Default)]
pub struct StartOfFrame {
    pub bits_per_sample: u8,
    //size of scan, x - number of samples per line  , y -number of lines
    pub size: ty2::Ty2<u16>,
    //number of components in frame
    //component - 2d array, part of image
    pub components: Vec<ComponentSpecificParams>,
}
impl StartOfFrame {
    pub fn new() -> StartOfFrame {
        StartOfFrame {
            ..Default::default()
        }
    }
}
#[derive(Clone, Copy, Default, Debug)]
pub struct ScanComponetntSpecific {
    pub component: u8,
    pub ac_coding_table: u8,
    pub dc_coding_table: u8,
}
#[derive(Clone, Default, Debug)]
pub struct StartOfScan {
    pub specific_per_scan: Vec<ScanComponetntSpecific>,
}
impl StartOfScan {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }
}
pub fn is_jpg(data: &mut Cursor<&[u8]>) -> bool {
    let mut marker: [u8; 2] = Default::default();
    data.read_exact(&mut marker).unwrap();
    if marker == [0xFF, 0xD8] {
        return true;
    }
    false
}
//skips appn(application data) marke
pub fn skip_appn(data: &mut Cursor<&[u8]>) {
    //test if marker is one of 16 appns
    for _ in 0..16 {
        let mut marker: [u8; 2] = Default::default();
        data.read_exact(&mut marker).unwrap();
        //checks if there is marker
        if marker[0] == 0xFF && (0xE0..0xEF).contains(&marker[1]) {
            //reads  appn size and skips
            let size = data.read_u16::<BigEndian>().unwrap() as i64;
            data.seek(SeekFrom::Current(size)).unwrap();
        } else {
            //returns to previous offset if appn not found
            data.seek(SeekFrom::Current(-2)).unwrap();
        }
    }
}
#[derive(Clone, Copy)]
pub struct QuantizationTable {
    pub elements: [u8; 64],
    pub id: u8,
}
impl Default for QuantizationTable {
    fn default() -> Self {
        Self {
            elements: [0; 64],
            id: 0,
        }
    }
}
pub fn read_quantization_tables(
    data: &mut Cursor<&[u8]>,
) -> Result<QuantizationTable, jpg_errors::JpgErr> {
    //detects marker
    let mut marker: [u8; 2] = Default::default();
    data.read_exact(&mut marker).unwrap();
    if marker != [0xFF, 0xDB] {
        return Err(jpg_errors::JpgErr::MarkerNotFound);
    }
    //skips size, tables have fixed sizes
    data.seek(SeekFrom::Current(2)).unwrap();
    let mut q = QuantizationTable::default();
    //reads id
    q.id = data.read_u8().unwrap() << 4;
    //reads data
    data.read_exact(&mut q.elements).unwrap();
    Ok(q)
}

#[derive(Clone, Copy, Debug)]
pub struct HuffanTable {
    pub id: u8,
    pub symbols: [u8; 162],
    pub codes: [u16; 162],
    pub offsets: [u32; 16],
    pub is_ac: bool,
}
impl HuffanTable {
    //decode huffman code
    //@returns false if symbol not found
    pub fn symbol_form_code(&self, code: u16, len: u8, symbol: &mut u8) -> bool {
        //there is only one code with length 1
        if len == 1 {
            //checks if 1 bit code exits
            if self.offsets[0] != 0 {
                //checks if code is valid
                if self.codes[0] == code {
                    //write symbol for given code
                    *symbol = self.symbols[0];
                    //success
                    return true;
                }
            }
        } else {
            //for longer codes, do linear search through all n bit codes
            for i in self.offsets[(len - 1) as usize]..self.offsets[len as usize] {
                if self.codes[i as usize] == code {
                    *symbol = self.symbols[i as usize];
                    return true;
                }
            }
        }
        //code not found
        false
    }
}
impl Default for HuffanTable {
    fn default() -> Self {
        Self {
            id: 0,
            symbols: [0; 162],
            codes: [0; 162],
            offsets: [0; 16],
            is_ac: false,
        }
    }
}
//create codes to fill huffman tree
/*eg.
               0
           00  01 11
         001  010 011
*/
pub fn create_huffman_codes(table: &mut HuffanTable) {
    let mut code: u32 = 0;
    //one bit codes
    for i in 0..table.offsets[0] {
        table.codes[i as usize] = code as u16;
        code += 1;
    }
    //shifts code to left with overflow
    code = code.checked_shl(1).unwrap_or(0);
    //generate all other codes
    for i in 0..15 as usize {
        for j in table.offsets[i]..table.offsets[i + 1] {
            table.codes[j as usize] = code as u16;
            code += 1;
        }
        code = code.checked_shl(1).unwrap_or(0);
    }
}
pub fn read_huffman_tables(
    data: &mut Cursor<&[u8]>,
) -> Result<[HuffanTable; 2], jpg_errors::JpgErr> {
    //read marker
    let mut marker: [u8; 2] = Default::default();
    data.read_exact(&mut marker).unwrap();
    if marker != [0xFF, 0xC4] {
        return Err(jpg_errors::JpgErr::MarkerNotFound);
    }
    //read size
    let size = data.read_u16::<BigEndian>().unwrap() as u64;
    let end = size + data.position() - 2;
    let mut huffmans: [HuffanTable; 2] = Default::default();
    //reads info about tables until end
    while data.position() < end {
        let info_byte = data.read_u8().unwrap();
        //ac and dc have separate huffman tables
        let is_ac = info_byte >> 4;
        //table id
        let id = info_byte & 0x0F;
        huffmans[id as usize].id = id;
        huffmans[id as usize].is_ac = is_ac != 0;
        let mut symbols_size: u32 = 0;
        // huffan tree codes number for each lenght from 1 to 16 bits
        for i in 0..16 {
            symbols_size += data.read_u8().unwrap() as u32;
            huffmans[id as usize].offsets[i] = symbols_size;
        }
        if symbols_size > 162 {
            return Err(jpg_errors::JpgErr::HuffmanTableErr);
        }
        //reads symbols
        for i in 0..symbols_size {
            huffmans[id as usize].symbols[i as usize] = data.read_u8().unwrap();
        }
        //create tree
        create_huffman_codes(&mut huffmans[id as usize]);
    }
    Ok(huffmans)
}

//reads comment
pub fn read_comment(data: &mut Cursor<&[u8]>) -> Result<String, jpg_errors::JpgErr> {
    //read marker
    let mut marker: [u8; 2] = Default::default();
    data.read_exact(&mut marker).unwrap();
    if marker == [0xFF, 0xFE] {
        //reads string size
        let size = data.read_u16::<BigEndian>().unwrap() - 2;
        let mut comment = String::new();
        unsafe {
            //reads string
            let vec = comment.as_mut_vec();
            vec.resize(size as usize, Default::default());
            data.read(vec.as_mut_slice()).unwrap();
        }
        return Ok(comment);
    }
    return Err(jpg_errors::JpgErr::MarkerNotFound);
}
//reads start of frame
pub fn read_sof_basline(data: &mut Cursor<&[u8]>) -> Result<StartOfFrame, jpg_errors::JpgErr> {
//reads marker
    let mut marker: [u8; 2] = Default::default();
    data.read_exact(&mut marker).unwrap();
    if marker != [0xFF, 0xC0] {
        return Err(jpg_errors::JpgErr::MarkerNotFound);
    }
    let mut header = StartOfFrame::new();
    //skips 2 bytes size. Size is always the same 
    data.seek(SeekFrom::Current(2)).unwrap();
    //alway 8
    header.bits_per_sample = data.read_u8().unwrap();
    //image size
    header.size.y = data.read_u16::<BigEndian>().unwrap();
    header.size.x = data.read_u16::<BigEndian>().unwrap();
    //number of components (1 for monochrmatic, 3 for color jpgs)
    let components_len = data.read_u8().unwrap() as usize;
    for _ in 0..components_len {
        let mut info = ComponentSpecificParams::new();
        info.component_id = data.read_u8().unwrap();
        let sampling: u8 = data.read_u8().unwrap();
        //subsampling factor
        info.hsample = sampling >> 4 & 0b1111;
        info.vsample = sampling & 0b1111;
        //qtable used in component (CrCb usually use other than Y)
        info.quantzation_table = data.read_u8().unwrap();
        header.components.push(info);
    }
    return Ok(header);
}
//start of scan header 
pub fn read_sos_beaseline(data: &mut Cursor<&[u8]>) -> Result<StartOfScan, jpg_errors::JpgErr> {
    let mut marker: [u8; 2] = Default::default();
    data.read_exact(&mut marker).unwrap();
    if marker != [0xFF, 0xDA] {
        return Err(jpg_errors::JpgErr::MarkerNotFound);
    }
    let mut start_of_scan = StartOfScan::new();
    data.seek(SeekFrom::Current(2)).unwrap();
    //components present in scan(in baseline jpg number same as in frame header)
    let components_in_scan = data.read_u8().unwrap();
    for _ in 0..components_in_scan {
        let mut component: ScanComponetntSpecific = Default::default();
        component.component = data.read_u8().unwrap();
        let tables = data.read_u8().unwrap();
        //huffamn tabel for  first coefficent
        component.ac_coding_table = tables >> 4 & 0b1111;
        //huffman table for 63 coefficencts
        component.dc_coding_table = tables & 0b1111;
        start_of_scan.specific_per_scan.push(component);
    }
    data.seek(SeekFrom::Current(3)).unwrap();
    return Ok(start_of_scan);
}
pub struct JpgBaseline {
    pub frame: StartOfFrame,
    pub sacn: StartOfScan,
    pub huffman: [HuffanTable; 2],
    pub quantization: [QuantizationTable; 4],
    pub data: Vec<u8>,
}
pub fn read_data(data: &mut Cursor<&[u8]>, jpg: &mut JpgBaseline) {
    data.read_to_end(&mut jpg.data).unwrap();
    jpg.data.pop();
    jpg.data.pop();
}
