use crate::utils::jpg_errors;
use crate::utils::ty2;

#[derive(Clone, Copy)]
pub struct Pixel<T, const C: usize> {
    channels: [T; C],
}
impl<T: std::clone::Clone + std::default::Default + std::marker::Copy, const C: usize> Pixel<T, C> {
    pub unsafe fn from_slice_unchecked(slice: &[T]) -> Self {
        Self {
            channels: slice[..C].try_into().unwrap_unchecked(),
        }
    }
}
impl<T: std::clone::Clone + std::default::Default + std::marker::Copy, const C: usize> Default
    for Pixel<T, C>
{
    fn default() -> Self {
        Self {
            channels:[Default::default();C]
        }
    }
}

#[derive(Clone)]
struct Bitmap<T, const C: usize> {
    size: ty2::Ty2<u16>,
    data: Vec<Pixel<T, C>>,
}

impl<T: std::clone::Clone + std::default::Default + std::marker::Copy, const C: usize>
    Bitmap<T, C>
{
    pub fn new(size: ty2::Ty2<u16>) -> Self {
        Self {
            size: size,
            data: vec![Default::default(); (size.prod()).into()],
        }
    }
    pub fn get(&self, coord: ty2::Ty2<u16>) -> Result<Pixel<T, C>, jpg_errors::JpgErr> {
        if coord.x > self.size.x || coord.y > self.size.y {
            return Err(jpg_errors::JpgErr::OutOfBitmapBounds);
        }
        unsafe { Ok(self.get_unchecked(coord)) }
    }
    pub unsafe fn get_unchecked(&self, coord: ty2::Ty2<u16>) -> Pixel<T, C> {
        *self
            .data
            .get_unchecked((coord.y * self.size.x + coord.x) as usize)
    }
    pub fn set(
        &mut self,
        coord: ty2::Ty2<u16>,
        pixel: Pixel<T, C>,
    ) -> Result<(), jpg_errors::JpgErr> {
        if coord.x > self.size.x || coord.y > self.size.y {
            return Err(jpg_errors::JpgErr::OutOfBitmapBounds);
        }
        unsafe {
            self.set_unchecked(coord, pixel);
        }
        Ok(())
    }
    pub unsafe fn set_unchecked(&mut self, coord: ty2::Ty2<u16>, pixel: Pixel<T, C>) {
        *self
            .data
            .get_unchecked_mut((coord.y * self.size.x + coord.x) as usize) = pixel;
    }
}
struct BitmapRef<'a, T, const C: usize> {
    bitmap: &'a mut Bitmap<T, C>,
    pos: ty2::Ty2<u16>,
    size: ty2::Ty2<u16>,
}
impl<'a, T: std::clone::Clone + std::default::Default + std::marker::Copy, const C: usize>
    BitmapRef<'a, T, C>
{
    pub unsafe fn new_unchecked(
        bitmap: &'a mut Bitmap<T, C>,
        coord: ty2::Ty2<u16>,
        size: ty2::Ty2<u16>,
    ) -> Self {
        Self {
            pos: coord,
            size: size,
            bitmap: bitmap,
        }
    }
    pub fn new(
        bitmap: &'a mut Bitmap<T, C>,
        coord: ty2::Ty2<u16>,
        size: ty2::Ty2<u16>,
    ) -> Result<Self, jpg_errors::JpgErr> {
        if coord.x + size.y > bitmap.size.x || coord.y + size.y > bitmap.size.y {
            return Err(jpg_errors::JpgErr::BitmapRefPosErr);
        }
        unsafe { Ok(BitmapRef::new_unchecked(bitmap, coord, size)) }
    }
    pub fn get(&self, coord: ty2::Ty2<u16>) -> Result<Pixel<T, C>, jpg_errors::JpgErr> {
        self.bitmap.get(coord)
    }
    pub unsafe fn get_unchecked(&self, coord: ty2::Ty2<u16>) -> Pixel<T, C> {
        self.bitmap.get_unchecked(coord)
    }
    pub fn set(
        &mut self,
        coord: ty2::Ty2<u16>,
        pixel: Pixel<T, C>,
    ) -> Result<(), jpg_errors::JpgErr> {
        self.bitmap.set(coord, pixel)
    }
    pub unsafe fn set_unchecked(&mut self, coord: ty2::Ty2<u16>, pixel: Pixel<T, C>) {
        self.bitmap.set_unchecked(coord, pixel)
    }
}

