use std::io::{Cursor, Seek, SeekFrom};

use byteorder::ReadBytesExt;

pub mod ty2 {
    #[derive(Clone, Copy, Default)]
    pub struct Ty2<T> {
        pub x: T,
        pub y: T,
    }
}

impl<T: std::ops::Mul<Output = T> + std::marker::Copy> ty2::Ty2<T> {
    pub fn prod(&self) -> T {
        self.x * self.y
    }
}
pub mod jpg_errors {
    use std::fmt;
    use std::fmt::Debug;
    #[derive(Debug, PartialEq, Eq)]
    pub enum JpgErr {
        MarkerNotFound,
        OutOfBitmapBounds,
        BitmapRefPosErr,
        NegativeSize,
        HuffanDataErr,
        HuffmanTableErr,
    }
    impl std::error::Error for JpgErr {}

    impl fmt::Display for JpgErr {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            match self {
                JpgErr::MarkerNotFound => write!(f, "Marker not found"),
                JpgErr::OutOfBitmapBounds => write!(f, "Coord is out of bounds"),
                JpgErr::BitmapRefPosErr => {
                    write!(f, "Bitmap refrence area is outide refred bitmap")
                }
                JpgErr::NegativeSize => write!(
                    f,
                    "first poition is behind second position that's makes negative size"
                ),
                JpgErr::HuffanDataErr => write!(f, "huffman data is corrupted"),
                JpgErr::HuffmanTableErr => write!(f, "humman table is corrupted"),
            }
        }
    }
}
pub fn read_bit(data: &mut Cursor<&[u8]>, bitoffset: &mut u8) -> u8 {
    let mut bit = data.read_u8().unwrap();
    bit = bit & (1 >> *bitoffset);
    *bitoffset += 1;
    if *bitoffset == 8 {
        *bitoffset = 0;
    } else {
        data.seek(SeekFrom::Current(-1)).unwrap();
    }
    bit
}
