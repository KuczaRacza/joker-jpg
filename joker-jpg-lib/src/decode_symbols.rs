use std::io::Cursor;

use crate::jpg_parse;
use crate::jpg_parse::HuffanTable;
use crate::utils::jpg_errors;
use crate::utils::read_bit;
use crate::utils::ty2;

pub struct EncodedMcu {
    pub ac: [u8; 3],
    pub dc: [u8; 3 * 63],
}
impl Default for EncodedMcu {
    fn default() -> Self {
        Self {
            ac: [0; 3],
            dc: [0; 3 * 63],
        }
    }
}
impl EncodedMcu {
    pub unsafe fn get_unchecked_dc(&self, component: u8, pos: ty2::Ty2<u32>) -> u8 {
        *self
            .dc
            .get_unchecked(((pos.y * 8 + pos.x * 3) - 3 + component as u32) as usize)
    }
}
pub fn decode_huffman_symbol(
    data: &mut Cursor<&[u8]>,
    bits_offset: &mut u8,
    huffman: &HuffanTable,
) -> u8 {
    let mut code = 0 as u16;
    let mut bitoffset = 0 as u8;
    for i in 1..17 {
        code = (code << 1) | read_bit(data, &mut bitoffset) as u16;
        let mut result = 0 as u8;
        if huffman.symbol_form_code(code, i, &mut &mut result){
            return  result;
        }
    }
    0
}
pub fn decode_huffan_data(
    data: &mut Cursor<&[u8]>,
    jpg: &jpg_parse::JpgBaseline,
) -> Result<Vec<EncodedMcu>, jpg_errors::JpgErr> {
    let mut mcus = Vec::<EncodedMcu>::new();
    let image_size = ty2::Ty2 {
        x: (jpg.frame.size.x + 7) / 8,
        y: (jpg.frame.size.y + 7) / 8,
    };
    let mut prev_dc: [u8; 3] = [0; 3];

    return Ok(mcus);
}
