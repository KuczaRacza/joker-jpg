pub mod bitmap;
pub mod decode_symbols;
pub mod jpg_parse;
pub mod utils;
#[cfg(test)]
mod tests {
    use crate::jpg_parse::{
        is_jpg, read_comment, read_huffman_tables, read_quantization_tables, read_sof_basline,
        read_sos_beaseline,
    };
    use crate::utils::jpg_errors;
    use std::fs::File;
    use std::io::SeekFrom;
    use std::io::{prelude::*, Cursor};
    #[test]
    fn parse_test() {
        let mut file =
            File::open("test_obj/Kitagawa_Marin_holding_The_Rust_Programming_Language.jpg")
                .unwrap();
        let mut filedata = Vec::<u8>::new();
        file.read_to_end(&mut filedata).unwrap();
        let content = filedata.as_mut_slice();
        let mut data = Cursor::new(content.as_ref());
        assert_eq!(is_jpg(&mut data), true, "jpg not detected");
        data.seek(SeekFrom::Start(0)).unwrap();

        assert_eq!(
            read_comment(&mut data).unwrap_err(),
            jpg_errors::JpgErr::MarkerNotFound,
            "Detect Marker, false positive"
        );
        data.seek(SeekFrom::Start(2)).unwrap();
        assert_eq!(
            read_comment(&mut data).unwrap(),
            "Lavc59.18.100\0",
            "Comment parsed incorrectrly"
        );
        data.seek(SeekFrom::Start(20)).unwrap();
        read_quantization_tables(&mut data).unwrap();
        data.seek(SeekFrom::Start(89)).unwrap();
        read_huffman_tables(&mut data).unwrap();
        data.seek(SeekFrom::Start(273)).unwrap();
        let header = read_sof_basline(&mut data).unwrap();
        assert_eq!(header.bits_per_sample, 8, "Image has 8 bits per sample");
        assert_eq!(header.components.len(), 3, "Image has 3 components");
        assert_eq!(header.components[0].hsample, 1, "horizotal res factor is 1");
        assert_eq!(header.components[0].vsample, 2, "vertical res factor is 2");
        assert_eq!(
            header.components[1].vsample, 2,
            "second component header was readed wrong"
        );
        assert_eq!(
            header.components[2].component_id, 3,
            "second component header was readed wrong"
        );
        assert_eq!(
            header.components[2].quantzation_table, 0,
            "quantzation_table is  not equal to 0"
        );
        data.seek(SeekFrom::Start(292)).unwrap();
        let scan_header = read_sos_beaseline(&mut data).unwrap();
        assert_eq!(
            scan_header.specific_per_scan.len(),
            3,
            "Wrong component  number"
        );
        assert_eq!(
            scan_header.specific_per_scan[0].component, 1,
            "Wrong component id"
        );
        assert_eq!(
            scan_header.specific_per_scan[0].ac_coding_table, 0,
            "Wrong ac_table"
        );
        assert_eq!(
            scan_header.specific_per_scan[1].ac_coding_table, 1,
            "Wrong ac_table"
        );
    }
}
