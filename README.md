 
## Konfiguracja Projektu

1. zainstaluj rust nightly toolchain [Instalacja](https://www.rust-lang.org/tools/install)
2. ``cd joker-jpg-lib && cargo run --release ``

## Postępy

- [x] Parsowanie pliku jpg
- [x] Dekodownaie kodu huffmana
- [ ] Zamiana YCrCb  na RGB
- [ ] IDCT
- [ ] Narzędzie CLI

